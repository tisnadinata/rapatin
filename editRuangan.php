﻿
						<div class="module">
							<div class="module-head">
								<h3>Form Edit Ruangan</h3>
							</div>
							<div class="module-body">
								<?php
									$result = explode("|",editRuangan());
									if(isset($result[0])){
										if($result[0] == "success"){
											echo '
												<div class="alert alert-success">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data tersimpan !</strong> Silahkan lihat di menu <a href="?page=informasiruangan" target="_blank">Daftar Ruangan</a>.
												</div>
											';										
										}else if($result[0] == "error"){
											echo '
												<div class="alert alert-error">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data gagal disimpan !</strong> Silahkan periksa kembali data yang anda masukan.<br>
													<strong>Kode Error : </strong> '.$result[1].'<br>
												</div>
											';										
/*										}else{
											echo '
												<div class="alert">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Terjadi kesalahan !</strong> Silahkan coba beberapa saat lagi !
												</div>
											';										
*/										}
									}
											$cubeaconID ="";
											$edit = explode("|",$_GET['edit']);
											$undangan="";
											$json_ruangan = getDataCollection("ruangan","select * where uuid='".$edit[1]."'");
											foreach($json_ruangan->entities as $ruangan){
												$json_gedung = getDataCollection("gedung","select * where uuid='".$ruangan->gedung."'");
												foreach($json_gedung->entities as $gedung){
													$nama_gedung = $gedung->name;
													$nama_ruangan = $ruangan->name;
													$kapasitas =$ruangan->kapasitas;
													$kota = $gedung->city;
													$lokasi = $gedung->location->latitude.",".$gedung->location->longitude;
													foreach($ruangan->cubeacon as $cubeacon){
														$cubeaconID .= $cubeacon.",";
													}
												}
											}
								?>
									<br/>
									<form action="" method="post" class="form-horizontal row-fluid">
										<input type="hidden" name="entity" value="<?php echo $edit[1]; ?>">
										<div class="control-group">
											<label class="control-label" for="basicinput">Nama Gedung</label>
											<div class="controls">
												<input type="text" id="nama_gedung" name="nama_gedung" value="<?php echo $nama_gedung;?>" placeholder="Nama lengkap gedung ( case sensitif )" class="span8" readonly>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Nama Ruangan</label>
											<div class="controls">
												<input type="text" name="nama_ruangan" id="nama_ruangan" value="<?php echo $nama_ruangan;?>" placeholder="Nama ruangan" class="span8" readonly>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Kapasitas</label>
											<div class="controls">
												<input type="number" id="kapasitas" name="kapasitas" value="<?php echo $kapasitas;?>" placeholder="0" min="0" class="span2" required>
												<span class="help-inline">orang</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Kota</label>
											<div class="controls">
												<input type="text" id="kota" name="kota" value="<?php echo $kota;?>" placeholder="Kota dimana gedung berada" class="span8" readonly>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Lokasi(Longitude,Latitude)</label>
											<div class="controls">
												<input type="text" name="lokasi" id="lokasi" value="<?php echo $lokasi;?>" placeholder="Lokasi gedung berupa Latitude dan Longitude" class="span8" readonly>
												<span class="help-inline">Pisahkan dengan , (koma)</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Cubeaocn ID</label>
											<div class="controls">
												<input type="text" id="cubeacon" name="cubeacon" value="<?php echo $cubeaconID;?>" placeholder="Cubeacon yang dipakai pada ruangan" class="span8" required>
												<span class="help-inline">Pisahkan dengan ; (semicolon)</span>
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" name="btnEditRuangan" class="btn-danger">Edit Ruangan</button>
											</div>
										</div>
									</form>
							</div>
						</div>
