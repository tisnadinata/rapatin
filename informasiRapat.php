                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Daftar Rapat</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No.
                                                </th>
                                                <th>
                                                    Nama Rapat
                                                </th>
                                                <th>
                                                    Atas Nama
                                                </th>
                                                <th>
                                                    Deskripsi Rapat
                                                </th>
                                                <th>
                                                    Ruangan
                                                </th>
                                                <th>
                                                    Tanggal / Waktu
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php 
											$i = 1;
											$json_rapat = getDataCollection("rapat","");
											foreach($json_rapat->entities as $rapat){
												$json_ruangan = getDataCollection("ruangan","select * where uuid=".$rapat->ruangan."");
												foreach($json_ruangan->entities as $ruangan){
													echo "
													<tr class=''>
														<td>
															".$i."
														</td>
														<td>
														".$rapat->name."
														</td>
														<td>
														".$rapat->atasnama."
														</td>
														<td>														
														".$rapat->deskripsi."
														</td>
														<td>														
													";
														$json_gedung = getDataCollection("gedung","select * where uuid=".$ruangan->gedung."");
														foreach($json_gedung->entities as $gedung){
															echo $gedung->city." / ".$gedung->name." / ".$ruangan->name;
														}
													echo"
														</td>
														<td class='center'>														
														".$rapat->tanggal."
														</td>
														<td class='center' style='width:40px;'>														
															<a href='?delete=rapat|".$rapat->uuid."' title='Hapus Rapat'><span class='menu-icon icon-trash'></span></a>
															|
															<a href='?edit=rapat|".$rapat->uuid."' title='Ubah Informasi Rapat'><span class='menu-icon icon-edit'></span></a>
														</td>
													</tr>
													";
												}
												$i++;
											}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--/.module-->
