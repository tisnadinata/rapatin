﻿<script>
function infoAbsensi(rapat){
	var dataString = 'info='+rapat;
    $.ajax({
        type: "POST",
        url: "ajax.php",
        data: dataString,
        cache: false,
        success: function(html) {
			$("#listAbsensi").html(html);
        }
    });
}
</script>

<div class="module">
	<div class="module-head">
	<h3>
		Informasi Umum</h3>
	</div>
	<div class="module-body">
			<div class="control-group">
				<label class="control-label" for="basicinput">Pilih Rapat</label>
				<div class="controls">
				<select tabindex="1" id="rapat" onChange="infoAbsensi(this.value)" name="rapat" data-placeholder="Nama Rapat" class="span4">
					<option value=''>-</option>
					<?php
						$json_rapat = getDataCollection("rapats","");
						foreach($json_rapat->entities as $rapat){
							$json_ruangan = getDataCollection("ruangan","select * where uuid=".$rapat->ruangan."");
							foreach($json_ruangan->entities as $ruangan){
								echo "
									<option value='".$rapat->uuid."'>".$ruangan->name." - ".$rapat->name."</option>
								";
							}													
						}													
					?>
				</select>
				<a href="javascript:printDiv('absensi');">
					<button type="submit" name="btnEditRapat" class="btn btn-danger pull-right">Cetak Absensi</button>
				</a>
				</div>
			</div>
			<hr>
			<div name="abseinsi" id="absensi">
				<CENTER>
					<h4>
						ABSENSI RAPAT
					</h4>
				</CENTER>
				<div id="listAbsensi">
				<table border="1" style="width:100%;">
					<thead style="background-color: bisque;">
						<th width='10px' style="text-align:center;">NO</th>
						<th width='200px' style="text-align:center;">NAMA LENGKAP</th>
						<th width='125px' style="text-align:center;">DIVISI BIDANG</th>
						<th width='125px' style="text-align:center;">EMAIL</th>
						<th width='200px' colspan="2" style="text-align:center;">TANDA TANGAN</th>
					</thead>
					<tbody>
						<tr>
							<td colspan='5' >&nbsp Pilih rapat terlebih dahulu</td>						
						</tr>
					</tbody>
				</table>
				</div>
			</div>
	</div>
</div>