﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Edmin</title>
	<link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link type="text/css" href="css/theme.css" rel="stylesheet">
	<link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
					<i class="icon-reorder shaded"></i>
				</a>

			  	<a class="brand" href="index.php">
			  		Cubeacon RAPATin
			  	</a>

			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->



	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="module module-login span4 offset4">
					<?php
						include 'function/functions.php';
						if(isset($_POST['btnDaftar'])){
							if(addUser() == 'sukses'){
								echo '
									<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>Selamat !</strong> Anda berhasil daftar, silahkan login <a href="login.php" target="_blank">disini</a>.
									</div>
								';										
							}else{
								echo '
									<div class="alert alert-error">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>Maaf!</strong> Anda gagal melakukan pendaftaran.<br>
									</div>
								`';		
							}
						}
					?>
					<form action="" method="POST" class="form-vertical">
						<div class="module-head">
							<h3>Register</h3>
						</div>
						<div class="module-body">
							<div class="control-group">
								<div class="controls row-fluid">
									Email :
									<input class="span12" type="email" id="inputEmail" name="inputEmail" placeholder="Email">
								</div>
							</div>
							<div class="control-group">
								<div class="controls row-fluid">
									Name :
									<input class="span12" type="text" id="inputName" name="inputName" placeholder="Name">
								</div>
							</div>
							<div class="control-group">
								<div class="controls row-fluid">
									Username :
									<input class="span12" type="text" id="inputUsername" name="inputUsername" placeholder="Username">
								</div>
							</div>
							<div class="control-group">
								<div class="controls row-fluid">
									Password :
									<input class="span12" type="password" id="inputPassword" name="inputPassword" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="module-foot">
							<div class="control-group">
								<div class="controls clearfix">
									<button type="submit" name="btnDaftar" class="btn btn-primary pull-right">DAFTAR</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div><!--/.wrapper-->

	<div class="footer">
		<div class="container">
		</div>
	</div>
	<script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>