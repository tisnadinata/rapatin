                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Daftar Ruangan</h3>
                                </div>
                                <div class="module-body table">
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No.
                                                </th>
                                                <th>
                                                    Gedung
                                                </th>
                                                <th>
                                                    Ruangan
                                                </th>
                                                <th>
                                                    Kapasitas
                                                </th>
                                                <th>
                                                    Kota
                                                </th>
                                                <th>
                                                    Lokasi
                                                </th>
                                                <th>
                                                    Cubeacon
                                                </th>
                                                <th>
													Aksi
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i = 1;
											$json_ruangan = getDataCollection("ruangan","");
											foreach($json_ruangan->entities as $ruangan){
												$json_gedung = getDataCollection("gedung","select * where uuid=".$ruangan->gedung."");
												foreach($json_gedung->entities as $gedung){
													echo "
													<tr>
														<td>
															".$i."
														</td>
														<td>
														".$gedung->name."
														</td>
														<td>
														".$ruangan->name."
														</td>
														<td class=center'>
														".$ruangan->kapasitas." orang
														</td>
														<td class=center'>
														".$gedung->city."
														</td>
														<td class='center'>
														<a href='".$gedung->gmaps."' target='_blank'>Lihat Map</a>
														</td>
														<td class='center'>														
														";
														foreach($ruangan->cubeacon as $cubeacon){
															echo "- ".$cubeacon."<br>";
														}
														echo"
														</td>
														<td class='center' style='width:40px;'>														
															<a href='?delete=ruangan|".$ruangan->uuid."' title='Hapus Ruangan'><span class='menu-icon icon-trash'></span></a>
															|
															<a href='?edit=ruangan|".$ruangan->uuid."' title='Ubah Informasi Ruangan'><span class='menu-icon icon-edit'></span></a>
														</td>
													</tr>
													";
												}
												$i++;
											}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--/.module-->
