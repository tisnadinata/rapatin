﻿
						<div class="module">
							<div class="module-head">
								<h3>Form Tambah Ruangan</h3>
							</div>
							<div class="module-body">
								<?php
									$result = explode("|",addRuangan());
									if(isset($result[0])){
										if($result[0] == "success"){
											echo '
												<div class="alert alert-success">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data tersimpan !</strong> Silahkan lihat di menu <a href="?page=informasiruangan" target="_blank">Daftar Ruangan</a>.
												</div>
											';										
										}else if($result[0] == "error"){
											echo '
												<div class="alert alert-error">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data gagal disimpan !</strong> Silahkan periksa kembali data yang anda masukan.<br>
													<strong>Kode Error : </strong> '.$result[1].'<br>
												</div>
											';										
/*										}else{
											echo '
												<div class="alert">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Terjadi kesalahan !</strong> Silahkan coba beberapa saat lagi !
												</div>
											';										
*/										}
									}
								?>
									<br/>
									<form action="" method="post" class="form-horizontal row-fluid">
										<div class="control-group">
											<label class="control-label" for="basicinput">Nama Gedung</label>
											<div class="controls">
												<input type="text" id="nama_gedung" name="nama_gedung" placeholder="Nama lengkap gedung ( case sensitif )" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Nama Ruangan</label>
											<div class="controls">
												<input type="text" name="nama_ruangan" id="nama_ruangan" placeholder="Nama ruangan" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Kapasitas</label>
											<div class="controls">
												<input type="number" id="kapasitas" name="kapasitas" placeholder="0" min="0" class="span2" required>
												<span class="help-inline">orang</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Kota</label>
											<div class="controls">
												<input type="text" id="kota" name="kota" placeholder="Kota dimana gedung berada" class="span8" required>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Lokasi(Latitude,Longitude)</label>
											<div class="controls">
												<input type="text" name="lokasi" id="lokasi" placeholder="Lokasi gedung berupa Latitude dan Longitude" class="span8" required>
												<span class="help-inline">Pisahkan dengan , (koma)</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Lokasi(Google Maps)</label>
											<div class="controls">
												<input type="text" name="gmaps" id="gmaps" placeholder="URL Lokasi dari google map" class="span8" required>
												<span class="help-inline">ex : https://goo.gl/maps/8QCQNJBFoU32</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Cubeaocn ID</label>
											<div class="controls">
												<input type="text" id="cubeacon" name="cubeacon" placeholder="Cubeacon yang dipakai pada ruangan" class="span8" required>
												<span class="help-inline">Pisahkan dengan ; (semicolon)</span>
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" name="btnSimpanRuangan" class="btn-danger">Simpan Ruangan</button>
											</div>
										</div>
									</form>
							</div>
						</div>
