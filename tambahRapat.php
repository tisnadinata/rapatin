﻿
						<div class="module">
							<div class="module-head">
								<h3>Form Tambah Rapat</h3>
							</div>
							<div class="module-body">
								<?php
									$result = explode("|",addRapat());
									if(isset($result[0])){
										if($result[0] == "success"){
											echo '
												<div class="alert alert-success">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data tersimpan !</strong> Silahkan lihat di menu <a href="?page=informasirapat" target="_blank">Daftar Rapat</a>.
												</div>
											';										
										}else if($result[0] == "error"){
											echo '
												<div class="alert alert-error">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data gagal disimpan !</strong> Silahkan periksa kembali data yang anda masukan.<br>
													<strong>Kode Error : </strong> '.$result[1].'<br>
												</div>
											';										
/*										}else{
											echo '
												<div class="alert">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Terjadi kesalahan !</strong> Silahkan coba beberapa saat lagi !
												</div>
											';										
*/										}
									}
								?>
									<br/>
									<form action="" method="post" class="form-horizontal row-fluid">
										<div class="control-group">
											<label class="control-label" for="basicinput">Atas Nama Pengajuan</label>
											<div class="controls">
												<input type="text" id="atas_nama" name="atas_nama" placeholder="Atas nama pengajuan rapat" class="span8">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Nama Rapat</label>
											<div class="controls">
												<input type="text" id="nama_rapat" name="nama_rapat" placeholder="Nama / topik rapat" class="span8">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Undang</label>
											<div class="controls">
												<input type="text" id="undangan" name="undangan" placeholder="Undang orang lain menghadiri rapat menggunakan email" class="span8">
												<span class="help-inline">Pisahkan dengan ; (semicolon)</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Deskripsi Rapat</label>
											<div class="controls">
												<textarea class="span8" rows="5" id="deskripsi" name="deskripsi" ></textarea>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Lokasi Rapat</label>
											<div class="controls">
												<select tabindex="1" id="ruangan" name="ruangan" data-placeholder="Lokasi Rapat" class="span8">
												<?php
													$json_ruangan = getDataCollection("ruangan","");
													foreach($json_ruangan->entities as $ruangan){
														$json_gedung = getDataCollection("gedung","select * where uuid=".$ruangan->gedung."");
														foreach($json_gedung->entities as $gedung){
															echo "
																<option value='".$ruangan->uuid."'>".$gedung->city." / ".$gedung->name." / ".$ruangan->name."</option>
															";
														}
													}													
												?>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Tanggal Rapat</label>
											<div class="controls">
												<input type="date" id="tanggal" name="tanggal" placeholder="" class="span4">
											</div>
										</div>
										<div class="control-group">
											<div class="span12">
												<label class="control-label" for="basicinput">Waktu</label>
												<div class="controls">
													<input type="time" id="mulai" name="mulai" placeholder="" class="span3">
													<span class="help-inline">sampai &nbsp </span>
													<input type="time" id="selesai"	name="selesai" placeholder="" class="span3">
												</div>
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" name="btnSimpanRapat" class="btn btn-danger">Simpan Rapat</button>
											</div>
										</div>
									</form>
							</div>
						</div>
