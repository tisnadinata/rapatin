<?php
	if(isset($_GET['delete'])){
		$delete = explode("|",$_GET['delete']);
		$collection = $delete[0];
		$uuid = $delete[1];
		$path = $collection."/".$uuid;
		$query= array(
			"?ql" => ""
		);
		$result = deleteEntity($path,$query);
		if($result){
			echo '
				<div class="alert alert-success">
					<strong>Data berhasil dihapus !</strong> Anda akan dialihkan.
				</div>
			';										
		}else{
			echo '
				<div class="alert alert-danger">
					<strong>Data gagal dihapus !</strong> Anda akan dialihkan.
				</div>
			';										
		}
		echo '<meta http-equiv="refresh" content="2; url=?page=informasi'.$collection.'" />';	
	}
?>