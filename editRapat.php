﻿
						<div class="module">
							<div class="module-head">
								<h3>Form Edit Rapat</h3>
							</div>
							<div class="module-body">
								<?php
									$result = explode("|",editRapat());
									echo $result[0];
									if(isset($result[0])){
										if($result[0] == "success"){
											echo '
												<div class="alert alert-success">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data tersimpan !</strong> Silahkan lihat di menu <a href="?page=informasirapat" target="_blank">Daftar Rapat</a>.
												</div>
											';										
										}else if($result[0] == "error"){
											echo '
												<div class="alert alert-error">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Data gagal disimpan !</strong> Silahkan periksa kembali data yang anda masukan.<br>
													<strong>Kode Error : </strong> '.$result[1].'<br>
												</div>
											';										
/*										}else{
											echo '
												<div class="alert">
													<button type="button" class="close" data-dismiss="alert">×</button>
													<strong>Terjadi kesalahan !</strong> Silahkan coba beberapa saat lagi !
												</div>
											';										
*/										}
									}
									$edit = explode("|",$_GET['edit']);
									$atasnama='';
									$nama_rapat='';
									$undangan="";
									$deskripsi='';
									$waktu='';
									$tanggal = '';
									$waktu = '';
									$mulai = '';
									$selesai = '';
									$kota = '';
									$gedung = '';
									$ruangan = '';
									$ruanganID = '';
									$json_rapat = getDataCollection("rapat","select * where uuid=".$edit[1]."");
									foreach($json_rapat->entities as $rapat){
										$json_ruangan = getDataCollection("ruangan","select * where uuid=".$rapat->ruangan."");
										foreach($json_ruangan->entities as $ruangan){
												$json_gedung = getDataCollection("gedung","select * where uuid=".$ruangan->gedung."");
												foreach($json_gedung->entities as $gedung){
													$atasnama=$rapat->atasnama;
													$nama_rapat=$rapat->name;
													foreach($rapat->undangan as $undang){
														$undangan.=$undang.";";
													}
													$deskripsi=$rapat->deskripsi;
													$waktu=explode(" / ",$rapat->tanggal);
													$tanggal = $waktu[0];
													$waktu = explode("-",$waktu[1]);
													$mulai = $waktu[0];
													$selesai = $waktu[1];
													$kota = $gedung->city;
													$gedung = $gedung->name;
													$ruangan = $ruangan->name;
													$ruanganID = $rapat->ruangan;
												}
										}
									}
								?>
									<br/>
									<form action="" method="post" class="form-horizontal row-fluid">
										<input type="hidden" name="entity" value="<?php echo $edit[1]; ?>">
										<div class="control-group">
											<label class="control-label" for="basicinput">Atas Nama Pengajuan</label>
											<div class="controls">
												<input type="text" id="atas_nama" name="atas_nama" value="<?php echo $atasnama;?>" placeholder="Atas nama pengajuan rapat" class="span8">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Nama Rapat</label>
											<div class="controls">
												<input type="text" id="nama_rapat" name="nama_rapat" value="<?php echo $nama_rapat;?>" placeholder="Nama / topik rapat" class="span8">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Undang</label>
											<div class="controls">
												<input type="text" id="undangan" name="undangan" value="<?php echo $undangan;?>" placeholder="Undang orang lain menghadiri rapat menggunakan email" class="span8">
												<span class="help-inline">Pisahkan dengan ; (semicolon)</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Deskripsi Rapat</label>
											<div class="controls">
												<textarea class="span8" rows="5" id="deskripsi" name="deskripsi" ><?php echo $deskripsi;?></textarea>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Lokasi Rapat</label>
											<div class="controls">
												<select tabindex="1" id="ruangan" name="ruangan" data-placeholder="Lokasi Rapat" class="span8">
												<?php
													echo "<option value='".$ruanganID."'>".$kota." / ".$gedung." / ".$ruangan."</option>";
													echo "<option value='".$ruanganID."'>--------------------------------------</option>";
													$json_ruangan = getDataCollection("ruangan","");
													foreach($json_ruangan->entities as $ruangan){
														$json_gedung = getDataCollection("gedung","select * where uuid=".$ruangan->gedung."");
														foreach($json_gedung->entities as $gedung){
															echo "
																<option value='".$ruangan->uuid."'>".$gedung->city." / ".$gedung->name." / ".$ruangan->name."</option>
															";
														}
													}													
												?>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Tanggal Rapat</label>
											<div class="controls">
												<input type="date" id="tanggal" name="tanggal" value="<?php echo $tanggal;?>" placeholder="" class="span4">
											</div>
										</div>
										<div class="control-group">
											<div class="span12">
												<label class="control-label" for="basicinput">Waktu</label>
												<div class="controls">
													<input type="time" id="mulai" name="mulai" value="<?php echo $mulai;?>" placeholder="" class="span3">
													<span class="help-inline">sampai &nbsp </span>
													<input type="time" id="selesai"	name="selesai" value="<?php echo $selesai;?>" placeholder="" class="span3">
												</div>
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" name="btnEditRapat" class="btn btn-danger">Edit Rapat</button>
											</div>
										</div>
									</form>
							</div>
						</div>
