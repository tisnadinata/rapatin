<?php
	session_start();
	include 'autoloader.inc.php';
	usergrid_autoload('Apache\\Usergrid\\Client');
	$client = new Apache\Usergrid\Client('https://api.nobackend.id','tbaas.rapatin','dirapat');
function login($username,$password){
	global $client;
	$login = $client->login($username,$password);
	if($login){
		echo '
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Login berhasil !</strong> anda akan dialihkan.
			</div>
		';
		
		$_SESSION['login'] = 1;
		echo $client->is_logged_in();
//		echo '<meta http-equiv="refresh" content="5; url=index.php" />';	
	}else{
		echo '
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Login gagal !</strong>.
			</div>
		';										
	}
}
function logout(){
	global $client;
	$client->log_out();
	if ($client->is_logged_in() == 1) {
		echo '<meta http-equiv="refresh" content="0; url=index.php" />';	
	} else {
		unset($_SESSION['login']);
		session_destroy();
		echo '<meta http-equiv="refresh" content="0; url=login.php" />';	
	 }
}
function getData($path){
	$base_url = "https://api.nobackend.id";
	$org = "tbaas.rapatin";
	$app = "dirapat";
	$get_content = file_get_contents($base_url."/".$org."/".$app."/".$path);
	return $get_content;
}
function getDataCollection($collection,$query){
	$get_content = getData($collection."/?ql=".urlencode($query));
	$result = json_decode($get_content);
	return $result;
}
function getDataEntity($collection,$entity,$query){
	$get_content = getData($collection."/".$entity."/?ql=".urlencode($query));
	$result = json_decode($get_content);
	return $result;
}
function getJumlahData($collection,$query){
	$jumlah = 0;
	$json_jumlah = getDataCollection($collection,$query);
	foreach($json_jumlah->entities as $data){
		$jumlah++;
	}
	return $jumlah;
}
function addEntity($collection,$data){
	global $client;
	$endpoint = $collection;
	$query_string = array();
	$body = $data;
	$result = $client->post($endpoint, $query_string, $body);
	if($result->get_error()){
		return FALSE;
	}else{
		return TRUE;
	}
}
function putEntity($path,$query,$data){
	global $client;
	$endpoint = $path;
	$query_string = $query;
	$body = $data;
	$result = $client->put($endpoint, $query_string, $body);
	if ($result->get_error()){
		return FALSE;
	}else{
		return TRUE;
	}
}
function getEntity($path,$query){
	global $client;
	$endpoint = $path;
	$query_string = $query;
	$result = $client->get($endpoint,$query_string);
	if ($result->get_error()){
		return $result;
	} else {
		return FALSE;
	}
}
function deleteEntity($path){
	global $client;
	$endpoint = $path;
	$query_string = array();
	$result =  $client->delete($endpoint, $query_string);
	if ($result->get_error()){
		return FALSE;
	   //error - there was a problem deleting the entity
	} else {
		return TRUE;
	   //success - entity deleted
	}
}
function addRuangan(){
	global $client;
	if(isset($_POST['btnSimpanRuangan'])){
		$nama_gedung	=$_POST['nama_gedung'];
		$nama_ruangan	=$_POST['nama_ruangan'];
		$kapasitas		=$_POST['kapasitas'];
		$kota			=$_POST['kota'];
		$gmaps			=$_POST['gmaps'];
		$lokasi			=explode(",",$_POST['lokasi']);
		$cubeacon		=explode(";",$_POST['cubeacon']);
		if(count($lokasi)!=2){
			return "error|Format lokasi tidak sesuai!";
			exit;
		}
		$cek_gedung = getDataCollection("gedung","select * where name='".$nama_gedung."' AND city='".$kota."'");
		if(count($cek_gedung->entities) > 0){
			$properties_ruangan = array(
				"name" => $nama_ruangan,
				"kapasitas" => $kapasitas,
				"gedung" => $cek_gedung->entities[0]->uuid,
				"cubeacon" => $cubeacon
			);
			$data = addEntity("ruangans",$properties_ruangan);
			if ($data){
				return "success";
			} else {
				return "error|Gagal menambah entity Ruangan!";
			}
		}else{
			$properties_gedung = array(
				"name" => $nama_gedung,
				"city" => $kota,
				"gmaps" => $gmaps,
				"location" => array(
						"latitude" => $lokasi[0],
						"longitude" => $lokasi[1]
				)
			);
			$data = addEntity("gedungs",$properties_gedung);
			if ($data){
				$stat = true;
			} else {
				return "error|Gagal membuat entity Gedung! (3) " .json_encode($properties_gedung);				
			}
			if($stat){
				sleep(3);
				$cek_gedung = getDataCollection("gedung","select * where name='".$nama_gedung."' AND city='".$kota."'");
				$uuid_gedung = $cek_gedung->entities[0]->uuid;
				$properties_ruangan = array(
					"name" => $nama_ruangan,
					"kapasitas" => $kapasitas,
					"gedung" => $uuid_gedung,
					"cubeacon" => $cubeacon
				);
				$data = addEntity("ruangans",$properties_ruangan);
				if ($data) {
					return "success";
				} else {
					return "error|Gagal menambah entity Ruangan!";
				}
			}
		}
	}
}
function editRuangan(){
	global $client;
	if(isset($_POST['btnEditRuangan'])){
		$entity			=$_POST['entity'];
		$nama_gedung	=$_POST['nama_gedung'];
		$nama_ruangan	=$_POST['nama_ruangan'];
		$kapasitas		=$_POST['kapasitas'];
		$kota			=$_POST['kota'];
		$lokasi			=explode(",",$_POST['lokasi']);
		$cubeacon		=explode(",",$_POST['cubeacon']);
		$properties_ruangan = array(
			"name" => $nama_ruangan,
			"kapasitas" => $kapasitas,
			"cubeacon" => $cubeacon
		);
		$query = array();
		$data = putEntity("ruangan/".$entity."",$query,$properties_ruangan);
		if ($data){
			return "success";
		} else {
			return "error|Gagal mengubah entity Ruangan!";
		}
	}
}
function addRapat(){
	global $client;
	if(isset($_POST['btnSimpanRapat'])){
		$atas_nama		=$_POST['atas_nama'];
		$nama_rapat		=$_POST['nama_rapat'];
		$undangan		=explode(";",$_POST['undangan']);
		$deskripsi		=$_POST['deskripsi'];
		$ruangan		=$_POST['ruangan'];
		$tanggal		=$_POST['tanggal'];
		$mulai			=$_POST['mulai'];
		$selesai		=$_POST['selesai'];
		$waktu = $tanggal." / ".$mulai."-".$selesai;
		$properties_rapat = array(
			"atasnama" => $atas_nama,
			"name" => $nama_rapat,
			"undangan" => $undangan,
			"deskripsi" => $deskripsi,
			"ruangan" => $ruangan,
			"tanggal" => $waktu
		);
		$data = addEntity("rapats",$properties_rapat);
		if ($data){
			return "success";
		} else {
			return "error|Gagal menambah entity Ruangan!";
		}
	}
}
function editRapat(){
	global $client;
	if(isset($_POST['btnEditRapat'])){
		$entity			=$_POST['entity'];
		$atas_nama		=$_POST['atas_nama'];
		$nama_rapat		=$_POST['nama_rapat'];
		$undangan		=explode(";",$_POST['undangan']);
		$deskripsi		=$_POST['deskripsi'];
		$ruangan		=$_POST['ruangan'];
		$tanggal		=$_POST['tanggal'];
		$mulai			=$_POST['mulai'];
		$selesai		=$_POST['selesai'];
		$waktu = $tanggal." / ".$mulai."-".$selesai;
		$properties_rapat = array(
			"atasnama" => $atas_nama,
			"name" => $nama_rapat,
			"undangan" => $undangan,
			"deskripsi" => $deskripsi,
			"ruangan" => $ruangan,
			"tanggal" => $waktu
		);
		$query = array();
		$data = putEntity("rapat/".$entity."",$query,$properties_rapat);
		if ($data){
			return "success";
		} else {
			return "error|Gagal mengubah entity Rapat!";
		}
	}
}
function getAbsensi($rapat){
	global $client;
	$query =  "select * where rapatID=".$rapat."";
	$json_absensi = getDataCollection("kehadiran",$query);
	echo '
	<table border="1" style="width:100%;">
					<thead style="background-color: bisque;">
						<th width="10px" style="text-align:center;">NO</th>
						<th width="200px" style="text-align:center;">NAMA LENGKAP</th>
						<th width="125px" style="text-align:center;">DIVISI BIDANG</th>
						<th width="125px" style="text-align:center;">EMAIL</th>
						<th width="200px" colspan="2" style="text-align:center;">TANDA TANGAN</th>
					</thead>
					<tbody>
	';						$i = 0;
							foreach($json_absensi->entities[0]->userID as $absensi){
								$query2 = "select * where uuid=".$absensi."";
								$json_user = getDataCollection("user",$query2);
									echo"
									<tr>
										<td align='center'>".($i+1)."</td>
										<td>&nbsp ".ucwords($json_user->entities[0]->name)."</td>
										<td style='text-align:center;'>".$json_user->entities[0]->divisi."</td>
										<td>&nbsp ".$json_user->entities[0]->email."</td>
									";			
										if(($i%2)==0){
											echo"
												<td width='100px'>".($i+1).".</td>
												<td width='100px'></td>
									</tr>
											";
										}else{
											echo"
												<td width='100px'></td>
												<td width='100px'>".($i+1).".</td>
									</tr>
											";
										}
								$i++;
							}
							if($i==0){
								echo"
								<tr>
									<td colspan='5' >&nbsp Data absensi masih kosong!</td>						
								</tr>
								";
							}
	echo"
					</tbody>
				</table>
	";
}
function addUser(){
	global $client;
	$properties = array(
		"name" => $_POST['inputName'],
		"email" => $_POST['inputEmail'],
		"password" => $_POST['inputPassword'],
		"username" => $_POST['inputUsername']
	);
	$data = addEntity("users",$properties);
	if($data){
		return "sukses";				
	} else {
		return "error";				
	}
}

?>